# Super-resolved Robust Principal Component Analysis (SRPCA)

## Table of Contents
- [Overview](#overview)
- [Requirements](#requirements)
- [Installation and Usage](#installation-and-usage)
  - [Installation](#installation)
  - [Usage](#usage)
- [SRPCA Paper Results](#srpca-paper-results)
- [Copyright / Warranty](#copyright--warranty)

## Overview

We present the codes for the SRPCA (Super-Resolved Robust Principal Component Analysis) paper.

SRPCA is intended to be used in conjunction with the code associated with the [OPUS-PALA article](https://github.com/AChavignon/PALA).

SRPCA is a deterministic method that enhances ultrasound localization microscopy (ULM) by improving the isolation and localization of overlapping microbubbles (MBs). It combines robust principal component analysis with complex-valued deconvolution, using the alternating direction method of multipliers optimization to address the joint problem of IQ deconvolution and RPCA with a measured system point spread function. This integration replaces the traditional ULM steps of tissue filtering, MB detection, and super-localization with a single super-resolution inverse problem. SRPCA effectively separates MB signals from background noise and enhances the resolution of the MB signals.

In the attached codes, SRPCA was applied to the rat brain dataset from the supplementary PALA dataset ([https://doi.org/10.5281/zenodo.7883227](https://doi.org/10.5281/zenodo.7883227)).

## Requirements

- **MATLAB**: R2020a or newer (Previous versions might work)
- **Required Toolboxes**: 
  - Communications
  - Bioinformatics
  - Image Processing
  - Curve Fitting
  - Signal Processing
  - Statistics and Machine Learning
  - Parallel Computing
  - Computer Vision Toolbox
- **RAM**: 64GB or more recommended for SRPCA

## Installation and Usage

### Installation

1. First, install the [OPUS-PALA](https://github.com/AChavignon/PALA) software.
2. Download the supplementary rat brain PALA dataset ([https://doi.org/10.5281/zenodo.7883227](https://doi.org/10.5281/zenodo.7883227)).
3. Decompress the zip files into a single folder.
4. Place the provided `PALA_InVivoULM_Supplementary.m` and `PALA_InVivoULM_SRPCA.m` scripts in the `PALA_scripts` folder.
5. Place the provided `SRPCA.m` and `ULM_localization2DSR.m` functions in the `PALA_addons` folder.
6. Modify file paths and locations in `PALA_InVivoULM_Supplementary.m` and `PALA_InVivoULM_SRPCA.m` to match the folder where you decompressed the rat brain data.

### Usage

- Run `PALA_InVivoULM_Supplementary.m` to compute the result of the ULM process (with radial symmetry super-localization) as presented in the PALA article, referred to as RS ULM in the SRPCA paper.
- Run `PALA_InVivoULM_SRPCA.m` to obtain the SRPCA outcome.

## SRPCA Paper Results

We provide the final rendering results of the rat brain dataset (Fig. 5 & 6):

- **SRPCA, SR=4, Full dataset (250 blocks)**: `SRPCAexample_matouts_250blocks.mat`
- **SRPCA, SR=4, First 40 blocks**: `SRPCAexample_matouts_40blocks.mat`
- **RS ULM, SR=10, Full dataset (250 blocks)**: `example_matouts_250blocks.mat`
- **RS ULM, SR=10, First 40 blocks**: `example_matouts_40blocks.mat`

## Copyright / Warranty

This code is for academic purposes only.

Copyright © 2024 Vassili Pustovalov, Duong Hung Pham, Denis Kouamé, IRIT Laboratory

Contact: [vassili.pustovalov@irit.fr](mailto:vassili.pustovalov@irit.fr)

MATLAB is a registered trademark of The MathWorks.

All PALA functions are usable with agreement from their owner.

The authors claim no responsibility for this software and code.